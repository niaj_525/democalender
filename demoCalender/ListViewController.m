//
//  ListViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/29/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "ListViewController.h"
#import "DBManager.h"
#import "ListCell.h"
#import "TimeSheet.h"
#import "DetailViewController.h"
#import "APIManager.h"
@interface ListViewController ()<UITableViewDataSource,UITableViewDelegate>{
    
    DBManager *db;
    NSMutableArray* listArray;
    NSMutableArray *dateArray;
    NSDateFormatter* formatter;
    int currentIndex;
     NSArray *sheetData;
    
    __weak IBOutlet UIButton *previousbtnAct;
    __weak IBOutlet UIButton *nextbtnAct;
    __weak IBOutlet UIView *containerV;
    
    __weak IBOutlet UILabel *datelbl;
    __weak IBOutlet UIScrollView *scrollV;
}
@property (nonatomic, strong) NSDate *monthShowing;
@property (weak, nonatomic) IBOutlet UITableView *listTableView;
@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db = [[DBManager alloc] init];
    listArray = [[NSMutableArray alloc] init];
    dateArray = [[NSMutableArray alloc] init];
    formatter = [[NSDateFormatter alloc] init];
    
    self.listTableView.delegate=self;
    self.monthShowing = [NSDate date];
    
    [self updateButtonStatus];
    
}

-(void) getcurrentdate{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSDate *date = [calendar dateFromComponents:components];
    NSRange days = [calendar rangeOfUnit:NSCalendarUnitDay
                                  inUnit:NSCalendarUnitMonth
                                 forDate:date];
    
    formatter.dateFormat = @"yyyy";
    NSString *currentyear = [formatter stringFromDate:[NSDate date]];
    
    formatter.dateFormat = @"MM";
    NSString *currentmonth = [formatter stringFromDate:[NSDate date]];
    
    
    [self getCallender:currentyear :currentmonth :days : nil];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromList"];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromHome"]) {
        NSLog(@"herere");
        
        NSMutableDictionary *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"SheetData"];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            DetailViewController* detail=[[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
            detail.sheetdict=data;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromList"];

            [self.navigationController pushViewController:detail animated:YES];
        }];
    }
    [self getcurrentdate];
    self.navigationController.navigationBarHidden=YES;
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromHome"];
}

-(void) updateButtonStatus{
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:self.monthShowing];
    [comps setSecond:0];
    
    NSDate *oldDate = [cal dateFromComponents:comps];
    
    NSDateComponents *comps2 = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:[NSDate date]];
    [comps2 setSecond:0];
    NSDate *currentDate = [cal dateFromComponents:comps2];

    
    if ([oldDate compare:currentDate] == NSOrderedSame) {
        nextbtnAct.enabled = NO;
        previousbtnAct.enabled = YES;
    }
    else{
        nextbtnAct.enabled = YES;
        previousbtnAct.enabled = YES;
 
    }
}

-(void)pushVC : (NSNotification *) info{
    
    sheetData = info.object;
    TimeSheet *sheet=[sheetData objectAtIndex:0];
    
    NSMutableDictionary* infoD=[[NSMutableDictionary alloc] init];
    [infoD setValue:sheet.date forKey:@"date"];
    [infoD setValue:sheet.checkinDate forKey:@"checkinDate"];
    [infoD setValue:sheet.checkoutDate forKey:@"checkoutDate"];
    [infoD setValue:sheet.checkinTime forKey:@"checkinTime"];
    [infoD setValue:sheet.checkoutTime forKey:@"checkoutTime"];
//    [infoD setValue:sheet.pauseInTime forKey:@"pauseInTime"];
//    [infoD setValue:sheet.pauseOutTime forKey:@"pauseOutTime"];
    [infoD setValue:sheet.monthName forKey:@"monthName"];
    [infoD setValue:sheet.yearName forKey:@"yearName"];
    [infoD setValue:sheet.memo forKey:@"memo"];
    [infoD setObject:sheet.pauseInfo forKey:@"pauseInfo"];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:infoD forKey:@"SheetData"];
    [userDefaults synchronize];
    //    [self performSelector:@selector(push:) withObject:sheetArr afterDelay:3];
}

//-(void) push : (NSArray *) sheetArr{
//    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        TimeSheet *sheet = [sheetArr objectAtIndex:0];
//        DetailViewController* detail=[[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
//        detail.sheet=sheet;
//        [self.navigationController pushViewController:detail animated:YES];
//    }];
//    
//}
#pragma mark Actions

- (IBAction)sendMail:(id)sender {
    NSLog(@"Send Email");
}


- (IBAction)previousbtnAct:(UIButton *)sender {
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:self.monthShowing];
    comps.month             -= 1;
    self.monthShowing          = [cal dateFromComponents:comps];
    
    NSRange days = [cal rangeOfUnit:NSCalendarUnitDay
                             inUnit:NSCalendarUnitMonth
                            forDate:self.monthShowing];
    
    formatter.dateFormat = @"yyyy";
    NSString *currentyear = [formatter stringFromDate:self.monthShowing];
    
    formatter.dateFormat = @"MM";
    NSString *currentmonth = [formatter stringFromDate:self.monthShowing];
    
    [self getCallender:currentyear :currentmonth :days : sender];
    
    [self updateButtonStatus];
}

- (IBAction)nextbtnAct:(UIButton *)sender {
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:self.monthShowing];
    comps.month             += 1;
    self.monthShowing          = [cal dateFromComponents:comps];
    
    NSRange days = [cal rangeOfUnit:NSCalendarUnitDay
                             inUnit:NSCalendarUnitMonth
                            forDate:self.monthShowing];
    
    formatter.dateFormat = @"yyyy";
    NSString *currentyear = [formatter stringFromDate:self.monthShowing];
    
    formatter.dateFormat = @"MM";
    NSString *currentmonth = [formatter stringFromDate:self.monthShowing];
    
    [self getCallender:currentyear :currentmonth :days : sender];
    
    [self updateButtonStatus];
}



-(void) getCallender : (NSString *) year : (NSString *) month : (NSRange) days : (UIButton *) btn{
    
    if (dateArray) {
        [dateArray removeAllObjects];
        [listArray removeAllObjects];
    }
    
    for (int i = 1;i<=days.length;i++){
        
        NSString *myDateString = [NSString stringWithFormat:@"%@-%@-%d",year,month,i];
        // Convert the string to NSDate
        formatter.dateFormat = @"yyyy-MM-dd";
        NSDate *date = [formatter dateFromString:myDateString];
        // Extract the day number (14)
        NSDateComponents *comp = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:date];
        NSInteger day = [comp day];
        // Extract the day name (Sunday)
        formatter.dateFormat = @"EEE";
        NSString *dayName = [formatter stringFromDate:date];
        // Print
        NSLog(@"Day: %ld: Name: %@", (long)day, dayName);
        [dateArray addObject:[NSString stringWithFormat:@"%ld(%@)",(long)day,dayName]];
        
    }
    
    /// place date label
    formatter.dateFormat = @"yyyy/MMM";
    NSString *datelblText = [formatter stringFromDate:self.monthShowing];
    datelbl.text = datelblText;

    
    formatter.dateFormat = @"yyyy";
    NSString *currentyear = [formatter stringFromDate:self.monthShowing];
    
    formatter.dateFormat = @"MMMM";
    NSString *currentmonth = [formatter stringFromDate:self.monthShowing];
    
    [listArray addObjectsFromArray:[db fetchList:currentmonth :currentyear]];

    CGRect frame = self.listTableView.bounds;
    frame.size.height = 72 * dateArray.count + 60;
    [self.listTableView setFrame:frame];
    
    scrollV.contentSize = CGSizeMake(self.view.frame.size.width, self.listTableView.frame.size.height + 20);
    
    if (btn.tag == 100) {
        self.listTableView.frame = CGRectMake(self.listTableView.frame.size.width,0, self.listTableView.frame.size.width, self.listTableView.frame.size.height);
        
    }
    else if (btn.tag == 101){
        self.listTableView.frame = CGRectMake(-self.listTableView.frame.size.width,0, self.listTableView.frame.size.width, self.listTableView.frame.size.height);
    }
    
    [UIView animateWithDuration:0.8f
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void)
     {
         
         self.listTableView.frame = CGRectMake(0, 0, self.listTableView.frame.size.width, self.listTableView.frame.size.height);
         
         self.listTableView.alpha = 1;
         
     }completion:NULL];
    
    [self.listTableView reloadData];
    
}

- (IBAction)dismissVC:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 25)];
    title.text = [NSString stringWithFormat:@"\t\t total hours worked 0.2h"];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setTextColor:[UIColor whiteColor]];
    
    [headerView setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:title];
    
    return headerView;
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
   
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 30)];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    title.text = [NSString stringWithFormat:@"\t\t total hours worked 0.2h"];
    [title setBackgroundColor:[UIColor clearColor]];
    [title setTextColor:[UIColor whiteColor]];
    
    [headerView setBackgroundColor:[UIColor clearColor]];
    [headerView addSubview:title];
    
    return headerView;
}

//-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    
//    return [NSString stringWithFormat:@"\t\ttotal hours worked 0.2h"];
//}
//
//-(NSString *) tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{
//    
//    return [NSString stringWithFormat:@"\t\ttotal hours worked 0.2h"];
//
//}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
        cell.backgroundColor =[UIColor colorWithRed:(30/256.0) green:(30/256.0) blue:(32/256.0) alpha:(1.0)];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 72;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return dateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        cell = (ListCell *)[topLevelObjects objectAtIndex:0];
        
//    }
    
    cell.datelbl.text=[dateArray objectAtIndex:indexPath.row];
    [cell.memoImg setHidden:YES];
    cell.accessoryType = UITableViewCellAccessoryNone;

    
    NSArray *filtered;
    for (TimeSheet *info in listArray) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"date" ,info.date];
        filtered = [listArray filteredArrayUsingPredicate:predicate];
        
    
    if (filtered.count) {
        TimeSheet *info=nil;
        info = (TimeSheet *)[filtered objectAtIndex:0];
        NSString *string = cell.datelbl.text;
        NSArray * arr;
        arr = [string componentsSeparatedByString:@"("];
        string=[arr objectAtIndex:0];
        
        arr = [info.date componentsSeparatedByString:@"/"];
        NSString *dateStr = [arr objectAtIndex:1];
        if ([dateStr hasPrefix:@"0"]) {
            dateStr = [dateStr stringByReplacingOccurrencesOfString: @"0" withString:@""];
        }
        if ([dateStr isEqualToString:string]) {
            NSArray * arr;
            arr = [info.checkinTime componentsSeparatedByString:@" "];
            NSString *checkin = [arr objectAtIndex:1];
            arr = [info.checkoutTime componentsSeparatedByString:@" "];
            NSString *checkout = [arr objectAtIndex:1];
            
            NSCalendar *cal         = [NSCalendar currentCalendar];
            NSDateComponents *comps = [cal components:NSCalendarUnitSecond
                                             fromDate:info.checkinDate];
            NSDate * current=[info.checkinDate dateByAddingTimeInterval:-comps.second];
            comps = [cal components:NSCalendarUnitSecond
                                              fromDate:info.checkoutDate];
            NSDate * current2=[info.checkoutDate dateByAddingTimeInterval:-comps.second];
            NSTimeInterval secondsBetween = (ceil([current2 timeIntervalSinceDate:current]));
            NSInteger ti = (NSInteger)secondsBetween;
            NSInteger minutes = (ti / 60) % 60;
            NSInteger hours = (ti / 3600);
            
            cell.timelbl.text =[NSString stringWithFormat:@"%@-%@ %@", checkin,checkout,[NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes]];
            
            
            NSLog(@"%@",info.memo);
            if (![info.memo isEqualToString:@""]) {
                [cell.memoImg setHidden:NO];
            }
        }
    
        filtered=nil;
    }
    }
    
    if ([nextbtnAct isEnabled]) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    else{
        formatter.dateFormat =@"dd";
        if (indexPath.row < [[formatter stringFromDate:[NSDate date]] intValue]) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        }
        else
            cell.accessoryType = UITableViewCellAccessoryNone;

    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:(47/256.0) green:(48/256.0) blue:(52/256.0) alpha:(1.0)];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return UITableViewCellEditingStyleDelete;
//}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    [tableView setEditing:YES animated:YES];
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BOOL hasInfo = true;
    
    for (TimeSheet *info in listArray) {
        NSArray * arr;
        arr = [[dateArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"("];
        NSString* cellLabel=[arr objectAtIndex:0];
        arr = [info.date componentsSeparatedByString:@"/"];
        NSString* infodate=[arr objectAtIndex:1];
        if ([infodate hasPrefix:@"0"]) {
            infodate = [infodate stringByReplacingOccurrencesOfString: @"0" withString:@""];
        }
        
        if ([infodate isEqualToString:cellLabel]) {
            DetailViewController* detail=[[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
            detail.sheet=info;
            
            
            [self.navigationController pushViewController:detail animated:YES];
            hasInfo = NO;
        }
    }
    
    if (hasInfo) {
        DetailViewController* detail=[[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
        //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"emptyCell"];
        APIManager *manager = [APIManager sharedManager];
        manager.emptyCell = YES;
        
        [self.navigationController pushViewController:detail animated:YES];
        
    }
}

@end
