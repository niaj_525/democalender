//
//  MoreViewController.h
//  demoCalender
//
//  Created by Niajul Hasan on 12/31/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeSheet.h"

@interface MoreViewController : UIViewController

@property (nonatomic, strong) TimeSheet *sheet;
@property (nonatomic, strong) NSMutableDictionary *sheetdict;
@property (nonatomic, strong) NSString *checktime;
@property (nonatomic, strong) NSString *navtitle;
@property (nonatomic, strong) NSDictionary *pauseTime;

@end
