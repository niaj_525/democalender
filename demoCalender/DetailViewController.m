//
//  DetailViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/30/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "DetailViewController.h"
#import "ListCell.h"
#import "MoreViewController.h"
#import "PauseViewController.h"
#import "AppDelegate.h"
#import "MemoViewController.h"
#import "DBManager.h"

@interface DetailViewController ()<UITableViewDataSource,UITableViewDelegate>{
   
    DBManager *db;
    NSDateFormatter *formatter;
    NSMutableArray *dataarray;
    int indexNum;
    int count;
    NSInteger ti;
    NSInteger ti2;
    NSMutableArray* checktimeArray;
    NSMutableArray *timeArray;

    __weak IBOutlet UIScrollView *scrollV;
    
}
@property (nonatomic, strong) NSDate *monthShowing;
@property (weak, nonatomic) IBOutlet UITableView *detailTable;
@end

@implementation DetailViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"Details";
    
    formatter = [[NSDateFormatter alloc] init];
    dataarray= [[NSMutableArray alloc] init];
    timeArray = [[NSMutableArray alloc] init];
    checktimeArray = [[NSMutableArray alloc] init];
    db = [[DBManager alloc] init];

    self.detailTable.delegate=self;
    self.detailTable.dataSource=self;
    self.detailTable.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
//    NSLog(@"%@",self.sheet);
    self.monthShowing = [NSDate date];

    UIBarButtonItem *delete = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deletefromDB)];
    
    self.navigationItem.rightBarButtonItem = delete;
    //    self.detailTable.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated {
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar  setBarTintColor:[UIColor blackColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:(250/256.0) green:(140/256.0) blue:(19/256.0) alpha:(1.0)]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"pauseInTime"];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"pauseOutTime"];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
        dataarray = [NSKeyedUnarchiver unarchiveObjectWithData:(NSData *)[self.sheetdict valueForKey:@"pauseInfo"]];
        [self totalWorked2:self.sheetdict];

    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"emptyCell"]){
        
        indexNum = 4;
        self.navigationItem.rightBarButtonItem.enabled = NO;
    }
    else{
        dataarray = [NSKeyedUnarchiver unarchiveObjectWithData:(NSData *)self.sheet.pauseInfo];
        [self totalWorked:self.sheet];

    }
    count=0;
    int flag=0;
    
    for (int i=0; i<dataarray.count; i++) {
        NSDictionary* pauseInfo = [dataarray objectAtIndex:i];
        NSString *str=[NSString stringWithFormat:@"%@",[pauseInfo valueForKey:@"pauseInTime"]];
        if (![str isEqualToString:@""]) {
            indexNum = 4 + (int)dataarray.count;
            if (flag==0) {
                [self totalPauseTime:dataarray];
                flag =1;
            }
        }
        else
            indexNum = 4;
    }
    
    
    CGRect frame = self.detailTable.bounds;
    frame.size.height = 72 * indexNum+300;
    [self.detailTable setFrame:frame];
    
    scrollV.contentSize = CGSizeMake(self.view.frame.size.width, self.detailTable.frame.size.height);
    
    [self.detailTable reloadData];
}

-(void) viewWillDisappear:(BOOL)animated{
    
   // [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"emptyCell"];
    
}

#pragma Delete Action

-(void) deletefromDB{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
        [db deleteData:[self.sheetdict valueForKey:@"date"]];
    }    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"emptyCell"]){
        
    }
    else{
        [db deleteData:self.sheet.date];
    }

    formatter.dateFormat=@"MMM/dd";
    NSString *currentTime= [formatter stringFromDate: [NSDate date]];
    NSString *checkFlag = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"FirstTimeCheckFlag"]];
    
    if([checkFlag isEqualToString:currentTime]){
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"FirstTimeCheckFlag"];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString *) getPreviousDayFormat {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    comps.day =-1;
    NSDate *previousDays = [calendar dateByAddingComponents:comps toDate:self.monthShowing options:0];
    formatter.dateFormat = @"MMM/dd";
    NSString *date = [formatter stringFromDate:previousDays];

    self.monthShowing=previousDays;

    return date;
}

-(NSString *) getNextDayFormat {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [NSDateComponents new];
    comps.day =+1;
    NSDate *nextDays = [calendar dateByAddingComponents:comps toDate:self.monthShowing options:0];
    
    formatter.dateFormat = @"MMM/dd";
    NSString *date = [formatter stringFromDate:nextDays];
    NSLog(@"%@",date);
    self.monthShowing=nextDays;
    return date;
}

- (IBAction)previousbtnAct:(id)sender {

   NSString* date = [self getPreviousDayFormat];
    NSArray *fetchArr = [db fetchDatausingDate:date];

}




- (IBAction)nextbtnAct:(id)sender {
    
   NSString* date = [self getNextDayFormat];
   
    NSArray *fetchArr = [db fetchDatausingDate:date];
}


-(void) totalPauseTime : (NSArray *) data{
    [timeArray removeAllObjects];
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    for (NSDictionary *info in data) {
        
        comps= [cal components:NSCalendarUnitSecond
                      fromDate:[info valueForKey:@"pauseInTime"]];
        NSDate * current=[[info valueForKey:@"pauseInTime"] dateByAddingTimeInterval:-comps.second];
        
        comps = [cal components:NSCalendarUnitSecond
                       fromDate:[info valueForKey:@"pauseOutTime"]];
        NSDate * current2=[[info valueForKey:@"pauseOutTime"] dateByAddingTimeInterval:-comps.second];
        
        NSTimeInterval secondsBetween = (ceil([current2 timeIntervalSinceDate:current]));
        [timeArray addObject:[NSString stringWithFormat:@"%ld",(long) secondsBetween]];
    }
    
     ti=0;
    for (int i=0; i<timeArray.count; i++) {
        ti += [[timeArray objectAtIndex:i] intValue];
        
    }
    
}


-(void) totalWorked : (TimeSheet *)info{
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    [checktimeArray removeAllObjects];
    
        comps= [cal components:NSCalendarUnitSecond
                      fromDate:info.checkinDate];
        NSDate * current=[info.checkinDate dateByAddingTimeInterval:-comps.second];
        
        comps = [cal components:NSCalendarUnitSecond
                       fromDate:info.checkoutDate];
        NSDate * current2=[info.checkoutDate dateByAddingTimeInterval:-comps.second];
        
        NSTimeInterval secondsBetween = (ceil([current2 timeIntervalSinceDate:current]));
        [checktimeArray addObject:[NSString stringWithFormat:@"%ld",(long) secondsBetween]];
    
    ti2=0;
    for (int i=0; i<checktimeArray.count; i++) {
        ti2 += [[checktimeArray objectAtIndex:i] intValue];
        
    }
}

-(void) totalWorked2 : (NSMutableDictionary *)info{
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    [checktimeArray removeAllObjects];

    comps= [cal components:NSCalendarUnitSecond
                  fromDate:[info valueForKey:@"checkinDate"]];
    NSDate * current=[[info valueForKey:@"checkinDate"] dateByAddingTimeInterval:-comps.second];
    
    comps = [cal components:NSCalendarUnitSecond
                   fromDate:[info valueForKey:@"checkoutDate"]];
    NSDate * current2=[[info valueForKey:@"checkoutDate"] dateByAddingTimeInterval:-comps.second];
    
    NSTimeInterval secondsBetween = (ceil([current2 timeIntervalSinceDate:current]));
    [checktimeArray addObject:[NSString stringWithFormat:@"%ld",(long) secondsBetween]];
    
    ti2=0;
    for (int i=0; i<checktimeArray.count; i++) {
        ti2 += [[checktimeArray objectAtIndex:i] intValue];
        
    }
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor =[UIColor colorWithRed:(30/256.0) green:(30/256.0) blue:(32/256.0) alpha:(1.0)];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section==1) {
        return @"HOURS WORKED";
    }
    else if (section==2)
        return @"MEMO";
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return indexNum;
    }
    else
        return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //    if (cell == nil) {
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
    cell = (ListCell *)[topLevelObjects objectAtIndex:0];
    
    //    }
    
    [cell.memoImg setHidden:YES];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:(47/256.0) green:(48/256.0) blue:(52/256.0) alpha:(1.0)];
    [cell setSelectedBackgroundView:bgColorView];

    
    if (indexPath.section==0) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
            NSArray * arr;
            arr = [[self.sheetdict valueForKey:@"checkinTime"] componentsSeparatedByString:@" "];
            NSString *checkin = [arr objectAtIndex:1];
            arr = [[self.sheetdict valueForKey:@"checkoutTime"] componentsSeparatedByString:@" "];
            NSString *checkout = [arr objectAtIndex:1];
            
            if (indexPath.row==0) {
                
                cell.datelbl.text=@"Check-in";
                cell.timelbl.text =checkin;
            }
            else if (indexPath.row==1) {
                
                cell.datelbl.text=@"Check-out";
                cell.timelbl.text =checkout;
            }
            else if (indexPath.row==2) {
                
                cell.datelbl.text=@"Break";
            }
            else if (indexPath.row<=indexNum-2) {
                if (count<dataarray.count) {
                    NSDictionary* pauseInfo = [dataarray objectAtIndex:count];
                    //            NSLog(@"%@",pauseInfo);
                    cell.datelbl.text=@"Pause";
                    
                    formatter.dateFormat = @"H:mm";
                    NSString *formatted=[formatter stringFromDate:[pauseInfo valueForKey:@"pauseInTime"]];
                    NSString *formatted2=[formatter stringFromDate:[pauseInfo valueForKey:@"pauseOutTime"]];
                    cell.timelbl.text =[NSString stringWithFormat:@"%@ - %@",formatted ,formatted2];
                    count++;
                }
            }
            else {
                cell.datelbl.text=@"Pause";
                
            }
        }
        else{
            
            NSArray * arr;
            arr = [self.sheet.checkinTime componentsSeparatedByString:@" "];
            NSString *checkin = [arr objectAtIndex:1];
            arr = [self.sheet.checkoutTime componentsSeparatedByString:@" "];
            NSString *checkout = [arr objectAtIndex:1];
            
            if (indexPath.row==0) {
                cell.datelbl.text=@"Check-in";
                cell.timelbl.text =checkin;
            }
            else if (indexPath.row==1) {
                cell.datelbl.text=@"Check-out";
                cell.timelbl.text =checkout;
            }
            else if (indexPath.row==2) {
                cell.datelbl.text=@"Break";
            }
            else if (indexPath.row<=indexNum-2) {
                if (count<dataarray.count) {
                    NSDictionary* pauseInfo = [dataarray objectAtIndex:count];
                    cell.datelbl.text=@"Pause";
                    
                    formatter.dateFormat = @"H:mm";
                    NSString *formatted=[formatter stringFromDate:[pauseInfo valueForKey:@"pauseInTime"]];
                    NSString *formatted2=[formatter stringFromDate:[pauseInfo valueForKey:@"pauseOutTime"]];
                    cell.timelbl.text =[NSString stringWithFormat:@"%@ - %@",formatted ,formatted2];
                    count++;
                }
            }
            else {
                cell.datelbl.text=@"Pause";
                
            }
        }
        return cell;
    }
    
    else if (indexPath.section==1){
        [cell.datelbl setHidden:YES];
        
        NSLog(@"%li %li",(long)ti,(long)ti2);
        NSInteger total = abs((int)ti - (int)ti2);
            NSInteger minutes = (total / 60) % 60;
            NSInteger hours = (total / 3600);
        
//            minutes = ((total/2) / 60) % 60;
//            hours = ((total/2) / 3600);
        
        //    [listArray addObject:[NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes]];
        cell.timelbl.text =[NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
    }
    else{
        [cell.datelbl setHidden:YES];
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
            cell.timelbl.text = [self.sheetdict valueForKey:@"memo"];
        }
        else
            cell.timelbl.text = self.sheet.memo;
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

        return cell;
    }
    return 0;
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row==0 || indexPath.row==1) {
            
            MoreViewController* detail=[[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
            if (indexPath.row==0) {
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
                    detail.checktime = [self.sheetdict valueForKey:@"date"];
                    //            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDetail"];
                    
                }else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"emptyCell"]){
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"cellEmptied"];
                }
                else
                    detail.checktime = self.sheet.date;
                
                    detail.navtitle = @"Check-in";
            }
            if (indexPath.row==1) {
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
                    detail.checktime = [self.sheetdict valueForKey:@"date"];
                    
                }    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"emptyCell"]){
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"cellEmptied"];

                }
                else
                    detail.checktime = self.sheet.date;
                
                detail.navtitle = @"Check-out";
            }
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
                
                detail.sheetdict=self.sheetdict;
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDetail"];
                
            }    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"emptyCell"]){
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"cellEmptied"];

            }
            else{
                detail.sheet=self.sheet;
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDetail"];
                
            }
            
            [self.navigationController pushViewController:detail animated:YES];
            
        }
        
        if (indexPath.row>=3) {
            PauseViewController  *detail = [[PauseViewController alloc] initWithNibName:@"PauseViewController" bundle:nil];
            
            //        ListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            //
            
            if (indexPath.row>indexNum-2) {
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
                    
                    detail.sheetdict=self.sheetdict;
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDetail"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isempty"];
                    
                }    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"emptyCell"]){
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDetail"];

                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isempty"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"cellEmptied"];


                }else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"emptyCell"]){
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDetail"];
                    
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isempty"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"cellEmptied"];

                }
                
                else {
                    
                    detail.sheet = self.sheet;
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDetail"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isempty"];
                    
                }
            }
            
            else{
                //            NSLog(@"%ld",(long)indexPath.row-3);
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
                    
                    detail.sheetdict=self.sheetdict;
                    detail.pauseTime = [dataarray objectAtIndex:indexPath.row-3];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDetail"];
                }else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"emptyCell"]){
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDetail"];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"cellEmptied"];

                    
                }else {
                    
                    detail.sheet=self.sheet;
                    detail.pauseTime = [dataarray objectAtIndex:indexPath.row-3];
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDetail"];
                    
                }
            }
            
            [self.navigationController pushViewController:detail animated:YES];
            
            
        }
    }
    
    else if (indexPath.section == 2){
        MemoViewController *memoVC = [[MemoViewController alloc] initWithNibName:@"MemoViewController" bundle:nil];
        
        NSString *dateString;
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
            memoVC.placeText = [self.sheetdict valueForKey:@"memo"];
            dateString = [self.sheetdict valueForKey:@"date"];
        }
        else{
            memoVC.placeText = self.sheet.memo;
            dateString = self.sheet.date;
        }

        memoVC.fromDetail = YES;
        memoVC.date = dateString;
        [self.navigationController pushViewController:memoVC animated:YES];
    }
    
}


@end
