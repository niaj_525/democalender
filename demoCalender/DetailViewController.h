//
//  DetailViewController.h
//  demoCalender
//
//  Created by Niajul Hasan on 12/30/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeSheet.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) TimeSheet *sheet;
@property (nonatomic, strong) NSMutableDictionary *sheetdict;

@end
