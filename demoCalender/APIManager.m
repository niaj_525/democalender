//
//  APIManager.m
//  demoCalender
//
//  Created by akanksha popli on 2/7/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import "APIManager.h"
static APIManager* sharedManger = nil;

@implementation APIManager

+ (APIManager*) sharedManager {
    
    if (!sharedManger) {
        sharedManger = [[APIManager alloc] init];
    }
    
    return sharedManger;
}

@end
