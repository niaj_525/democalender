//
//  TimePickerViewController.h
//  demoCalender
//
//  Created by Niajul Hasan on 1/29/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimePickerViewController : UIViewController

@property (nonatomic, strong) NSString *navTitle;
@property (nonatomic, strong) NSDate *pickerDate;
@property (nonatomic, strong) NSString *breakStr;
@end
