//
//  DBManager.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/22/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "DBManager.h"
#import "AppDelegate.h"
#import "TimeSheet.h"

@implementation DBManager

- (id) init {
    self = [super init];
    
    if (self) {
      appDalegate = [[UIApplication sharedApplication] delegate];
    }
    
    return self;
}
- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

-(NSArray *) fetchList : (NSString *) month : (NSString *) year{
    
    NSError *error;
    NSManagedObjectContext *context = [self managedObjectContext];

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"TimeSheet" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(monthName contains[cd] %@) AND (yearName contains[cd] %@)", month, year];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setReturnsObjectsAsFaults:NO];
    [fetchRequest setEntity:entity];
   
    // sort fetched data
    NSArray *scanA= [context executeFetchRequest:fetchRequest error:&error ];
//    for (TimeSheet* sheet in scanA) {
//        NSLog(@"%@ %@ %@ %@ %@\n",sheet.date,sheet.checkinTime,sheet.checkoutTime,sheet.monthName,sheet.yearName);
//    }
    [appDalegate updateHistoryArray:scanA];
    return scanA;
}


-(NSArray *) fetchDatausingCheckTime : (NSString *) checkTime {
    
    NSError *error;
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"TimeSheet" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"checkoutTime" ,checkTime];
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    [fetchRequest setEntity:entity];
    
    // sort fetched data
    NSArray *scanA= [context executeFetchRequest:fetchRequest error:&error ];
    for (TimeSheet* sheet in scanA) {
        NSLog(@"%@ %@ %@ %@ %@\n",sheet.date,sheet.checkinTime,sheet.checkoutTime,sheet.monthName,sheet.yearName);
    }
    
    return scanA;
}

-(NSArray *) fetchDatausingDate : (NSString *) date {
    
    NSError *error;
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"TimeSheet" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"date" ,date];
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    [fetchRequest setEntity:entity];
    
    // sort fetched data
    NSArray *scanA= [context executeFetchRequest:fetchRequest error:&error ];
    for (TimeSheet* sheet in scanA) {
        NSLog(@"%@ %@ %@ %@ %@\n",sheet.date,sheet.checkinTime,sheet.checkoutTime,sheet.monthName,sheet.yearName);
    }
    
    return scanA;
}

-(void) insertNewObject : (NSMutableDictionary *) dict{
    // insert to DB
    NSManagedObjectContext *context = [self managedObjectContext];
    
    
    TimeSheet *sheet = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"TimeSheet"
                                    inManagedObjectContext:context];
    
    //ADD RESULTS TO DATABASE
    sheet.date = [dict valueForKey:@"date"];
    sheet.checkinTime = [dict valueForKey:@"checkIN"];
    sheet.monthName=[dict valueForKey:@"currentMonth"];
    sheet.yearName=[dict valueForKey:@"currentYear"];
    sheet.checkinDate=[dict valueForKey:@"checkinDate"];
    sheet.checkoutTime=[dict valueForKey:@"checkOut"];
    sheet.checkoutDate=[dict valueForKey:@"checkoutDate"];
    sheet.memo = [dict valueForKey:@"memo"];
//    if ([dict objectForKey:@"pauseInfo"]) {
    sheet.pauseInfo=[dict objectForKey:@"pauseInfo"];
//    sheet.pauseInTime=[dict valueForKey:@"pauseInTime"];
//    sheet.pauseOutTime=[dict valueForKey:@"pauseOutTime"];
//    }
    NSError *error;
    if (![context save:&error]) {
    }
    
    [self fetchList:[dict valueForKey:@"currentMonth"] :[dict valueForKey:@"currentYear"]];

}

/// Update Existing Data
-(void) updateExistingData : (NSString *) date : (NSDictionary *) dict{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"TimeSheet" inManagedObjectContext:[self managedObjectContext]]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"date" ,date];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    if (results.count>0) {
        
        NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
        if ([dict valueForKey:@"checkOut"]) {
            [favoritsGrabbed setValue:[dict valueForKey:@"checkOut"] forKey:@"checkoutTime"];
            [favoritsGrabbed setValue:[dict valueForKey:@"checkoutDate"] forKey:@"checkoutDate"];

        }
        if ([dict valueForKey:@"pauseTime"]) {
            [favoritsGrabbed setValue:[dict valueForKey:@"pauseTime"] forKey:@"pauseTime"];
        }
        if ([dict valueForKey:@"memo"]) {
            [favoritsGrabbed setValue:[dict valueForKey:@"memo"] forKey:@"memo"];
        }
        if ([dict valueForKey:@"checkinTime"]) {
            [favoritsGrabbed setValue:[dict valueForKey:@"checkinTime"] forKey:@"checkinTime"];
            [favoritsGrabbed setValue:[dict valueForKey:@"checkinDate"] forKey:@"checkinDate"];
        }
        
        if (![[self managedObjectContext] save:&error]) {
        }
                
    }
}

/// Update Existing Data
-(void) updatePauseData : (NSString *) date : (NSData *) data{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"TimeSheet" inManagedObjectContext:[self managedObjectContext]]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"date" ,date];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [[self managedObjectContext] executeFetchRequest:request error:&error];
    if (results.count>0) {
        
        NSManagedObject* favoritsGrabbed = [results objectAtIndex:0];
        if (data) {
            [favoritsGrabbed setValue:data forKey:@"pauseInfo"];
            
        }
        
        if (![[self managedObjectContext] save:&error]) {
        }
        
    }
}

// delete data
-(void) deleteData : (NSString *) date{
    NSError *error;
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"TimeSheet" inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@",@"date" ,date];
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    [fetchRequest setEntity:entity];
    
    // sort fetched data
    NSArray *scanA= [context executeFetchRequest:fetchRequest error:&error ];
    for (NSManagedObject *obj in scanA) {
        NSLog(@" date %@ \n checkinTime %@ \n checkoutTime %@ monthName %@",[obj valueForKey:@"date"],[obj valueForKey:@"checkinTime"],[obj valueForKey:@"checkoutTime"],[obj valueForKey:@"monthName"]);
        [context deleteObject:obj];
        NSError* error = nil;
        [context save:&error];
    }
}

@end
