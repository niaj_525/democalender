//
//  AppDelegate.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/21/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "SettingViewController.h"
#import "ListViewController.h"
#import "TotalViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.historyArray = [[NSMutableArray alloc] init];
    
    /*Check for Sorting Key*/
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSObject * Autoplay = [prefs objectForKey:@"checked"];
    if(Autoplay!= nil){
    }else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"checked"];
    }
    NSObject * breakisOn = [prefs objectForKey:@"breakisOn"];
    if(breakisOn!= nil){
    }else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"breakisOn"];
    }
    NSObject * wageisOn = [prefs objectForKey:@"wageisOn"];
    if(wageisOn!= nil){
    }else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"wageisOn"];
    }
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"pauseInTime"];
    
    HomeViewController *homeVC=[[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    
    UINavigationController* nav=[[UINavigationController alloc] initWithRootViewController:homeVC];
    nav.tabBarItem.title=@"Home";
    
    ListViewController *listVC=[[ListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    
    UINavigationController* nav2=[[UINavigationController alloc] initWithRootViewController:listVC];
    nav2.tabBarItem.title=@"View";
    
        TotalViewController *totalVC=[[TotalViewController alloc] initWithNibName:@"TotalViewController" bundle:nil];
    
        UINavigationController* nav3=[[UINavigationController alloc] initWithRootViewController:totalVC];
        nav3.tabBarItem.title=@"Total";

    
        SettingViewController *settingVC=[[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    
        UINavigationController* nav4=[[UINavigationController alloc] initWithRootViewController:settingVC];
        nav4.tabBarItem.title=@"Setting";
    
    UITabBarController* tabC=[[UITabBarController alloc] init];
    tabC.viewControllers=@[nav,nav2,nav3,nav4];
    
  
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"OpenSans" size:11.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [tabC.tabBar setTintColor:[UIColor colorWithRed:(250/256.0) green:(140/256.0) blue:(19/256.0) alpha:(1.0)]];
    [tabC.tabBar setBackgroundColor:[UIColor blackColor]];
    [tabC.tabBar setBarStyle:UIBarStyleBlack];
    [[[[tabC tabBar] items] objectAtIndex:0] setImage:[[self imageWithImage:[UIImage imageNamed:@"home"] scaledToSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [[[[tabC tabBar] items] objectAtIndex:1] setImage:[[self imageWithImage:[UIImage imageNamed:@"view"] scaledToSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

    [[[[tabC tabBar] items] objectAtIndex:2] setImage:[[self imageWithImage:[UIImage imageNamed:@"total"] scaledToSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [[[[tabC tabBar] items] objectAtIndex:3] setImage:[[self imageWithImage:[UIImage imageNamed:@"setting"] scaledToSize:CGSizeMake(25, 25)] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabC.tabBar setTranslucent:YES];
    
    
    
//    [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
    
//    //    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:(250/256.0) green:(140/256.0) blue:(19/256.0) alpha:(1.0)]];
//    [[UINavigationBar appearance] setTitleTextAttributes:
//     [NSDictionary dictionaryWithObjectsAndKeys:
//      [UIFont fontWithName:@"OpenSans" size:18],
//      NSFontAttributeName,                                            [UIColor whiteColor],NSForegroundColorAttributeName,
//      nil]];
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"memo"] isEqualToString:@""]){
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"memo"];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = tabC;
    [self.window makeKeyAndVisible];
    // Override point for customization after application launch.
    return YES;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark Update History Array

-(void)updateHistoryArray: (NSArray *) historyAr{
    
    [self.historyArray removeAllObjects];
    [self.historyArray addObjectsFromArray:historyAr];
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"timesheet" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"timesheet.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
