//
//  TimeSheet+CoreDataProperties.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/22/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TimeSheet+CoreDataProperties.h"

@implementation TimeSheet (CoreDataProperties)

@dynamic date;
@dynamic checkinTime;
@dynamic checkoutTime;
@dynamic pauseInTime;
@dynamic pauseOutTime;
@dynamic memo;
@dynamic monthName;
@dynamic yearName;
@dynamic checkinDate;
@dynamic checkoutDate;
@dynamic pauseInfo;
@end
