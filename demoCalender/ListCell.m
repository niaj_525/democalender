//
//  ListCell.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/29/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "ListCell.h"

@implementation ListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
