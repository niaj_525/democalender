//
//  SettingViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/27/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "SettingViewController.h"
#import "TimePickerViewController.h"
#import "BreakTimeViewController.h"
#import "PickerViewController.h"
#import "SettingCell.h"


@interface SettingViewController ()<UITextFieldDelegate>{
    
    NSDateFormatter *formatter;
}

@property (weak, nonatomic) IBOutlet UITableView *settingTable;
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    formatter = [[NSDateFormatter alloc] init];
    
    self.settingTable.delegate=self;
    self.settingTable.dataSource=self;
    
    self.title = @"Setting";
    
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar  setBarTintColor:[UIColor blackColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:(250/256.0) green:(140/256.0) blue:(19/256.0) alpha:(1.0)]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
}

-(void) viewWillAppear:(BOOL)animated{
    [self.settingTable reloadData];
}

#pragma mark TableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor =[UIColor colorWithRed:(30/256.0) green:(30/256.0) blue:(32/256.0) alpha:(1.0)];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"GENERAL SETTINGS";

    }
    else if (section == 1){
        return @"SEND TO";

    }else if (section == 2){
        return @"MORE FUNCTIONS";
        
    }
    else
        return @"OTHERS";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 5;
        
    }
    else if (section == 1){
        return 1;
        
    }else if (section == 2){
        return 1;
        
    }
    else
        return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SettingCell" owner:self options:nil];
        cell = (SettingCell *)[topLevelObjects objectAtIndex:0];
        
    }
    
    formatter.dateFormat = @"HH:mm";
    
    if (indexPath.section == 0) {
        
        cell.mailTxtField.enabled = NO;
        
        if (indexPath.row==0) {
            NSDate *current = [[NSUserDefaults standardUserDefaults]objectForKey:@"startingTime"];
            cell.startinglbl.text = @"Starting Time";
            cell.finishlbl.text = [formatter stringFromDate:current];
            
        }
        else if (indexPath.row==1) {
            
            cell.startinglbl.text=@"Finish Time";
            NSDate *current = [[NSUserDefaults standardUserDefaults]objectForKey:@"finishingTime"];
            cell.finishlbl.text = [formatter stringFromDate:current];
        }
        else if (indexPath.row==2) {
            
            cell.startinglbl.text = @"Set break time";
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"breakisOn"]) {
                
                if ([[NSUserDefaults standardUserDefaults]objectForKey:@"startBreak"]) {
                    NSDate *start = [[NSUserDefaults standardUserDefaults]objectForKey:@"startBreak"];
                    NSDate *finish = [[NSUserDefaults standardUserDefaults]objectForKey:@"finishBreak"];
                    
                    cell.finishlbl.text = [NSString stringWithFormat:@"%@ - %@", [formatter stringFromDate:start],[formatter stringFromDate:finish]];
                    
                }
                else
                    cell.finishlbl.text = @"";
            }
            else
                cell.finishlbl.text = @"Off";
        }
        else if (indexPath.row==3) {
            
            //get scan data from array
            cell.startinglbl.text=@"deadline";
            cell.finishlbl.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"deadlineDate"];
        }
        else{
            cell.startinglbl.text=@"Set hourly wage";
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"wageisOn"]) {
                if ([[NSUserDefaults standardUserDefaults]valueForKey:@"wage"]) {
                    
                    cell.finishlbl.text=[[NSUserDefaults standardUserDefaults]valueForKey:@"wage"];
                }
                else
                    cell.finishlbl.text = @" ";
                
            }
            else
                cell.finishlbl.text = @"Off";
 
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section == 1) {
        
        cell.mailTxtField.enabled = YES;
        
        
        if (indexPath.row==0) {
            
            cell.startinglbl.hidden = YES;
            cell.finishlbl.hidden = YES;
            cell.mailTxtField.delegate =self;
            cell.mailTxtField.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"email"];
            
        }
        
    }
    else if (indexPath.section == 2) {
        
        cell.mailTxtField.enabled = NO;
        
        
        if (indexPath.row==0) {
            
            cell.startinglbl.hidden = NO;
            cell.finishlbl.hidden = YES;
            cell.mailTxtField.text = @"";
            cell.startinglbl.text=@"Purchase Add-On";
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section == 3) {
        
        cell.mailTxtField.enabled = NO;
        
        
        if (indexPath.row==0) {
            
            cell.startinglbl.hidden = NO;
            cell.finishlbl.hidden = YES;
            cell.mailTxtField.text = @"";
            cell.startinglbl.text=@"Support";
        }
        else{
            cell.finishlbl.hidden = YES;
            cell.startinglbl.text=@"About this application";
        }
        
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {

    if (indexPath.row==0 || indexPath.row==1) {
        dispatch_async(dispatch_get_main_queue(), ^{
            TimePickerViewController *pickerVC = [[TimePickerViewController alloc] initWithNibName:@"TimePickerViewController" bundle:nil];
            if (indexPath.row == 0) {
                pickerVC.navTitle = @"Starting Time";
                pickerVC.pickerDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"startingTime"];
            }else{
                pickerVC.navTitle = @"Finishing Time";
                pickerVC.pickerDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"finishingTime"];
            }
            
            [self.navigationController pushViewController:pickerVC animated:YES];

        });
       
    }
       else if (indexPath.row == 2) {
           dispatch_async(dispatch_get_main_queue(), ^{
               BreakTimeViewController *pickerVC = [[BreakTimeViewController alloc] initWithNibName:@"BreakTimeViewController" bundle:nil];
               pickerVC.navTitle = @"Set break Time";
               [self.navigationController pushViewController:pickerVC animated:YES];
           });
       }
       else if (indexPath.row == 3) {
           dispatch_async(dispatch_get_main_queue(), ^{
               PickerViewController *pickerVC = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
               pickerVC.navTitle = @"Deadline";
               [self.navigationController pushViewController:pickerVC animated:YES];
           });
       }
       else {
           dispatch_async(dispatch_get_main_queue(), ^{
               BreakTimeViewController *pickerVC = [[BreakTimeViewController alloc] initWithNibName:@"BreakTimeViewController" bundle:nil];
               pickerVC.navTitle = @"Set hourly wage";
               pickerVC.fromWhere = @"Wage";
               [self.navigationController pushViewController:pickerVC animated:YES];
           });
       }
        
    }
}


#pragma mark TextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSString *textFieldString = textField.text;
    if (textFieldString) {
        [[NSUserDefaults standardUserDefaults]setValue:textFieldString forKey:@"email"];
    }
    [self.settingTable reloadData];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end
