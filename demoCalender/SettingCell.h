//
//  SettingCell.h
//  demoCalender
//
//  Created by Niajul Hasan on 1/29/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *startinglbl;
@property (weak, nonatomic) IBOutlet UILabel *finishlbl;
@property (weak, nonatomic) IBOutlet UITextField *mailTxtField;

@end
