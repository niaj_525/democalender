//
//  APIManager.h
//  demoCalender
//
//  Created by akanksha popli on 2/7/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimeSheet.h"
@interface APIManager : NSObject

+(APIManager*) sharedManager;

@property (nonatomic, retain) TimeSheet *timeSheet;
@property (nonatomic, assign) BOOL emptyCell;

@end
