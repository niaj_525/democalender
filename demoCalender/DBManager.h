//
//  DBManager.h
//  demoCalender
//
//  Created by Niajul Hasan on 12/22/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface DBManager : NSObject{
    AppDelegate *appDalegate;

}

-(NSArray *) fetchDatausingCheckTime : (NSString *) checkTime;
-(NSArray *) fetchList : (NSString *) month : (NSString *) year;
-(NSArray *) fetchDatausingDate : (NSString *) date;
-(void) insertNewObject : (NSMutableDictionary *) dict;
-(void) updateExistingData : (NSString *) date : (NSDictionary *) dict;
-(void) updatePauseData : (NSString *) date : (NSData *) data;
-(void) deleteData : (NSString *) date;
@end
