//
//  HomeViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/22/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "HomeViewController.h"
#import "MemoViewController.h"
#import "DBManager.h"

@interface HomeViewController (){
    
    __weak IBOutlet UIButton *composebtn;
    __weak IBOutlet UILabel *checkInlbl;
    __weak IBOutlet UIButton *checkOutbtn;
    __weak IBOutlet UIButton *checkInbtn;
    __weak IBOutlet UILabel *currentDatelbl;
    __weak IBOutlet UILabel *currentTimelbl;
    __weak IBOutlet UIButton *pauseBtn;
    
    NSDateFormatter *dateFormatter;
    DBManager *db;
}

@end

@implementation HomeViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    db=[[DBManager alloc] init];
    self.listVC = [[ListViewController alloc] init];
    
    [self getLabelTime];
    [self getCurrentTimeAndDate];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self.listVC selector:@selector(pushVC:) name:@"pushVC" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSysTimeChanged:)
                                                 name:UIApplicationSignificantTimeChangeNotification
                                               object:nil];
    
    [self setNeedsStatusBarAppearanceUpdate];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden=YES;
    composebtn.enabled=NO;
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"check"] isEqualToString:@"Check-in"]) {
        pauseBtn.hidden=NO;
        pauseBtn.enabled=YES;
        checkInbtn.hidden=YES;
        checkInbtn.enabled=NO;
        checkOutbtn.enabled=YES;
        checkOutbtn.hidden=NO;
        composebtn.enabled=YES;
    }else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"check"] isEqualToString:@"Pause"]){
        
        pauseBtn.hidden=YES;
        pauseBtn.enabled=NO;
        checkInbtn.hidden=NO;
        checkInbtn.enabled=YES;
        [checkInbtn setTitle:@"Resume" forState:UIControlStateNormal];
        checkOutbtn.enabled=NO;
        checkOutbtn.hidden=YES;
        composebtn.enabled=NO;
    }else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"check"] isEqualToString:@"Resume"]){
        
        pauseBtn.hidden=NO;
        pauseBtn.enabled=YES;
        checkInbtn.hidden=YES;
        checkInbtn.enabled=NO;
        checkOutbtn.enabled=YES;
        checkOutbtn.hidden=NO;
        composebtn.enabled=YES;
    }
    else{
        
        pauseBtn.hidden=YES;
        pauseBtn.enabled=NO;
        checkInbtn.hidden=NO;
        checkInbtn.enabled=YES;
        checkOutbtn.enabled=NO;
        checkOutbtn.hidden=YES;
        composebtn.enabled=NO;
    }
}

-(void) viewWillDisappear:(BOOL)animated{
    //    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"pauseInTime"];
    //    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"pauseOutTime"];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void) handleSysTimeChanged: (NSNotification*) notification
{
    NSLog(@"time changed");
}

-(void) getCurrentTimeAndDate{
    [NSTimer scheduledTimerWithTimeInterval:1.0
                                     target:self
                                   selector:@selector(targetMethod:)
                                   userInfo:nil
                                    repeats:YES];
    
    dateFormatter.dateFormat=@"yyyy/MMM/dd(EEE)";
    NSString *currentTime= [dateFormatter stringFromDate: [NSDate date]];
    currentDatelbl.text=currentTime;
}

-(void) getLabelTime{
    
    
    checkInlbl.text=[NSString stringWithFormat:@"%@(%@)", [[NSUserDefaults standardUserDefaults] valueForKey:@"check"],[[NSUserDefaults standardUserDefaults] valueForKey:@"checkOUTtime"]];
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"check"]) {
        checkInlbl.text=@"You haven't checked in yet";
    }
    
}

-(void)targetMethod:(id)sender
{
    dateFormatter.dateFormat=@"H:mm";
    NSString *currentTime= [dateFormatter stringFromDate: [NSDate date]];
    
    currentTimelbl.text=currentTime;
    
}

- (void) UpdateButtonStatus {
    
    NSDate *currentDate = [NSDate date];
    [dateFormatter setDateFormat:@"ss"];
    
    int currentTimeSeconds = [dateFormatter stringFromDate:currentDate].intValue;
    
    [self performSelector:@selector(updateSelector) withObject:nil afterDelay:60 - currentTimeSeconds];
}

-(void) updateSelector {
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"check"] isEqualToString:@"Check-in"]) {
        pauseBtn.enabled =YES;
        checkOutbtn.enabled = YES;
    }
    else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"check"] isEqualToString:@"Pause"]) {
        checkInbtn.enabled =YES;
    }
    else if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"check"] isEqualToString:@"Resume"]){
        pauseBtn.enabled = YES;
        checkOutbtn.enabled =YES;
    }
    
}

- (IBAction)pausebtnAct:(id)sender {
    
    dateFormatter.dateFormat=@"H:mm";
    NSString *currentTime= [dateFormatter stringFromDate: [NSDate date]];
    NSDate *currentDate = [dateFormatter dateFromString:currentTime];
    
    dateFormatter.dateFormat=@"MMM/dd H:mm";
    NSString *currentlabel= [dateFormatter stringFromDate: [NSDate date]];
    
    [[NSUserDefaults standardUserDefaults] setValue:currentlabel forKey:@"checkOUTtime"];
    [[NSUserDefaults standardUserDefaults] setObject:currentDate forKey:@"pauseInTime"];
    [[NSUserDefaults standardUserDefaults] setValue:@"Pause" forKey:@"check"];
    
    [self getLabelTime];
    
    [checkInbtn setTitle:@"Resume" forState:UIControlStateNormal];
    
    pauseBtn.hidden=YES;
    pauseBtn.enabled=NO;
    checkInbtn.hidden=NO;
    checkInbtn.enabled=NO;
    checkOutbtn.enabled=NO;
    checkOutbtn.hidden=YES;
    
    [self UpdateButtonStatus];
}

- (IBAction)checkOutAct:(id)sender {
    
    
    pauseBtn.hidden=YES;
    pauseBtn.enabled=NO;
    checkInbtn.hidden=NO;
    checkInbtn.enabled=YES;
    checkOutbtn.enabled=NO;
    checkOutbtn.hidden=YES;
    composebtn.enabled=NO;
    [checkInbtn setTitle:@"Check-in" forState:UIControlStateNormal];
    
    
    dateFormatter.dateFormat=@"MMM/dd H:mm";
    NSString *currentTime= [dateFormatter stringFromDate: [NSDate date]];
    [[NSUserDefaults standardUserDefaults] setValue:currentTime forKey:@"checkOUTtime"];
    
    NSMutableDictionary* demoinfo= [[NSUserDefaults standardUserDefaults] objectForKey:@"CheckedIninfo"];
    [[NSUserDefaults standardUserDefaults] setValue:@"Check-out" forKey:@"check"];
    
    //First Time Check Flag
    dateFormatter.dateFormat=@"MMM/dd";
    NSString *currentTimeForCheckFlag= [dateFormatter stringFromDate: [NSDate date]];
    [[NSUserDefaults standardUserDefaults] setValue:currentTimeForCheckFlag forKey:@"FirstTimeCheckFlag"];
    //    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"checked"];
    
    
    NSMutableDictionary* info=[[NSMutableDictionary alloc] init];
    NSMutableArray* pauseArr=[[NSMutableArray alloc] init];
    NSData *arrayData;
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"pauseInTime"]!=nil){
        [info setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"pauseInTime"] forKey:@"pauseInTime"];
        [info setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"pauseOutTime"] forKey:@"pauseOutTime"];
        [pauseArr addObject:info];
        arrayData = [NSKeyedArchiver archivedDataWithRootObject:pauseArr];
        
        
    }
    else{
        [info setObject:@"" forKey:@"pauseInTime"];
        [info setObject:@"" forKey:@"pauseOutTime"];
        [pauseArr addObject:info];
        arrayData = [NSKeyedArchiver archivedDataWithRootObject:pauseArr];
    }
    
    [info setValue:[demoinfo valueForKey:@"checkIN"] forKey:@"checkIN"];
    [info setValue:[demoinfo valueForKey:@"date"] forKey:@"date"];
    [info setValue:[demoinfo valueForKey:@"currentMonth"] forKey:@"currentMonth"];
    [info setValue:[demoinfo valueForKey:@"currentYear"] forKey:@"currentYear"];
    [info setValue:[demoinfo valueForKey:@"checkinDate"] forKey:@"checkinDate"];
    [info setValue:currentTime forKey:@"checkOut"];
    [info setValue:[NSDate date] forKey:@"checkoutDate"];
    
    
    if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"memo"] isEqualToString:@""]){
        [info setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"memo"]
                forKey:@"memo"];
    }
    else
        [info setValue:@"" forKey:@"memo"];
    
    if (arrayData) {
        [info setObject:arrayData forKey:@"pauseInfo"];
        //        [info setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"pauseInTime"] forKey:@"pauseInTime"];
        //        [info setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"pauseOutTime"] forKey:@"pauseOutTime"];
    }
    
    [db insertNewObject:info];
    [self getLabelTime];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"memo"];
    
}

- (IBAction)checkInAct:(UIButton *)sender {
    
    dateFormatter.dateFormat=@"MMM/dd";
    NSString *currentTime= [dateFormatter stringFromDate: [NSDate date]];
    NSString *checkFlag = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"FirstTimeCheckFlag"]];
    
    if(![checkFlag isEqualToString:currentTime]){
        if ([sender.titleLabel.text isEqualToString:@"Resume"]) {
            
            
            pauseBtn.hidden=NO;
            pauseBtn.enabled=NO;
            checkInbtn.hidden=YES;
            checkInbtn.enabled=NO;
            checkOutbtn.enabled=NO;
            checkOutbtn.hidden=NO;
            
            [self UpdateButtonStatus];
            
            dateFormatter.dateFormat=@"H:mm";
            NSString *currentTime= [dateFormatter stringFromDate: [NSDate date]];
            NSDate *currentDate = [dateFormatter dateFromString:currentTime];
            [[NSUserDefaults standardUserDefaults] setObject:currentDate forKey:@"pauseOutTime"];
            dateFormatter.dateFormat=@"MMM/dd H:mm";
            NSString *currentlabel= [dateFormatter stringFromDate: [NSDate date]];
            
            [[NSUserDefaults standardUserDefaults] setValue:currentlabel forKey:@"checkOUTtime"];
            [[NSUserDefaults standardUserDefaults] setValue:@"Resume" forKey:@"check"];
            [self getLabelTime];
            
        }
        else{
            // Create AlertController
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Checking in?" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction *checkAction = [UIAlertAction actionWithTitle:@"Check In" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                // save data
                pauseBtn.hidden=NO;
                pauseBtn.enabled=NO;
                checkInbtn.hidden=YES;
                checkInbtn.enabled=NO;
                checkOutbtn.enabled=NO;
                checkOutbtn.hidden=NO;
                composebtn.enabled=YES;
                
                [self UpdateButtonStatus];
                
                [[NSUserDefaults standardUserDefaults] setValue:@"Check-in" forKey:@"check"];
                
                dateFormatter.dateFormat=@"MMM/dd H:mm";
                NSString *currentTime= [dateFormatter stringFromDate: [NSDate date]];
                dateFormatter.dateFormat=@"MMM/dd";
                NSString *currentDate= [dateFormatter stringFromDate: [NSDate date]];
                dateFormatter.dateFormat=@"MMMM";
                NSString *currentMonth= [dateFormatter stringFromDate: [NSDate date]];
                dateFormatter.dateFormat=@"yyyy";
                NSString *currentYear= [dateFormatter stringFromDate: [NSDate date]];
                [[NSUserDefaults standardUserDefaults] setValue:currentDate forKey:@"checkinTime"];
                [[NSUserDefaults standardUserDefaults] setValue:currentTime forKey:@"checkOUTtime"];
                
                NSMutableDictionary* info=[[NSMutableDictionary alloc] init];
                [info setValue:currentTime forKey:@"checkIN"];
                [info setValue:currentDate forKey:@"date"];
                [info setValue:currentMonth forKey:@"currentMonth"];
                [info setValue:currentYear forKey:@"currentYear"];
                [info setValue:[NSDate date] forKey:@"checkinDate"];
                
                [[NSUserDefaults standardUserDefaults] setObject:info forKey:@"CheckedIninfo"];
                
                [self getLabelTime];
                
            }];
            [alert addAction:checkAction];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    else{
        // Create AlertController
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"You can only check-in once in a day" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *checkAction = [UIAlertAction actionWithTitle:@"Change today's check-in time" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                NSLog(@"change today");
                
                NSArray* data = [db fetchDatausingCheckTime:[[NSUserDefaults standardUserDefaults] valueForKey:@"checkOUTtime"]];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"pushVC" object:data];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromHome"];
                
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"changeCheckTime"];
                [self.tabBarController setSelectedIndex:1];
                
                
            }];
            
            
        }];
        [alert addAction:checkAction];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
        }];
        [alert addAction:cancelAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}


- (IBAction)composebtnAct:(id)sender {
    MemoViewController *memoVC=[[MemoViewController alloc] initWithNibName:@"MemoViewController" bundle:nil];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"memo"]);
    if(![[[NSUserDefaults standardUserDefaults] valueForKey:@"memo"] isEqualToString:@""]){
        memoVC.placeText = [[NSUserDefaults standardUserDefaults] valueForKey:@"memo"];
    }
    [self.navigationController pushViewController:memoVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
