//
//  BreakTimeViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 1/30/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import "BreakTimeViewController.h"
#import "TimePickerViewController.h"
#import "SettingCell.h"

@interface BreakTimeViewController ()<UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>{

    NSDateFormatter *formatter;

    __weak IBOutlet UISwitch *breakSwitch;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation BreakTimeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.navTitle;
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithTitle:@"Finish" style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    
    self.navigationItem.rightBarButtonItem = save;
    
    formatter = [[NSDateFormatter alloc] init];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.tableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
}

-(void) viewWillAppear:(BOOL)animated{
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"breakisOn"] || [[NSUserDefaults standardUserDefaults] boolForKey:@"wageisOn"]) {
        
        [breakSwitch setOn:YES animated:YES];
        self.tableView.hidden = NO;
        [self.tableView reloadData];

    }
    else{
        [breakSwitch setOn:NO animated:YES];
        self.tableView.hidden = YES;

    }
    
//    NSDate *date = self.pickerDate;
//    NSLog(@"%@",date);
//    if (date) {
//        [self.timePicker setDate:self.pickerDate animated:YES];
//        
//    }
//    else
//        [self.timePicker setDate:[NSDate date] animated:YES];
    
}

-(void) saveTapped{
//    if ([self.fromWhere isEqualToString:@"Wage"]) {
////        [[NSUserDefaults standardUserDefaults] setObject:self.timePicker.date forKey:@"startingTime"];
//        
//    }
//    else
////        [[NSUserDefaults standardUserDefaults] setObject:self.timePicker.date forKey:@"finishingTime"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)switchChanged:(id)sender {
    
    if ([breakSwitch isOn]) {
        if ([self.fromWhere isEqualToString:@"Wage"]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"wageisOn"];

        }
        else
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"breakisOn"];
        
        [self.tableView setHidden:NO];
    }
    else{
        if ([self.fromWhere isEqualToString:@"Wage"]) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"wageisOn"];
            
        }
        else
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"breakisOn"];
        
        [self.tableView setHidden:YES];

    }
    
}


#pragma mark TableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor =[UIColor colorWithRed:(30/256.0) green:(30/256.0) blue:(32/256.0) alpha:(1.0)];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.fromWhere isEqualToString:@"Wage"]) {
        return 1;
    }
    else
        return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SettingCell" owner:self options:nil];
        cell = (SettingCell *)[topLevelObjects objectAtIndex:0];
        
    }
    
    formatter.dateFormat = @"HH:mm";
    
        cell.mailTxtField.enabled = NO;
    
    if (indexPath.row==0) {
        if ([self.fromWhere isEqualToString:@"Wage"]) {
            cell.startinglbl.text=@"Set hourly wage";
            cell.mailTxtField.enabled = YES;
            cell.finishlbl.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"wage"];
            cell.mailTxtField.delegate =self;
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        else{
            cell.startinglbl.text=@"Start break";
            NSDate *current = [[NSUserDefaults standardUserDefaults]objectForKey:@"startBreak"];
            cell.finishlbl.text = [formatter stringFromDate:current];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
    }
    else if (indexPath.row==1) {
        NSDate *current = [[NSUserDefaults standardUserDefaults]objectForKey:@"finishBreak"];
        cell.startinglbl.text = @"End break";
        cell.finishlbl.text = [formatter stringFromDate:current];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }

//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    
//    formatter.dateFormat = @"HH:mm";
//    
//    if (indexPath.row==0) {
//        NSDate *current = [[NSUserDefaults standardUserDefaults]objectForKey:@"startBreak"];
//        cell.textLabel.text = @"Start break";
//        cell.detailTextLabel.text = [formatter stringFromDate:current];
//        
//    }
//    else if (indexPath.row==1) {
//        
//        cell.textLabel.text=@"End break";
//        NSDate *current = [[NSUserDefaults standardUserDefaults]objectForKey:@"finishBreak"];
//        cell.detailTextLabel.text = [formatter stringFromDate:current];
//    }
//    
//    [cell.textLabel setTextColor:[UIColor whiteColor]];
//    [cell.detailTextLabel setTextColor:[UIColor whiteColor]];
   return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

            dispatch_async(dispatch_get_main_queue(), ^{
                TimePickerViewController *pickerVC = [[TimePickerViewController alloc] initWithNibName:@"TimePickerViewController" bundle:nil];
                if (indexPath.row == 0) {
                    pickerVC.navTitle = @" ";
                    pickerVC.breakStr = @"Start";
                    pickerVC.pickerDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"startBreak"];
                }else{
                    pickerVC.navTitle = @" ";
                    pickerVC.breakStr = @"Finish";
                    pickerVC.pickerDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"finishBreak"];
                }
                [self.navigationController pushViewController:pickerVC animated:YES];
                
            });
            
}


#pragma mark TextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSString *textFieldString = textField.text;
    if (textFieldString) {
        [[NSUserDefaults standardUserDefaults] setValue:textFieldString forKey:@"wage"];
    }
    [self.tableView reloadData];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
