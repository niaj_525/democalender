//
//  TotalViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 1/5/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import "TotalViewController.h"
#import "DBManager.h"
#import "ListCell.h"
#import "TimeSheet.h"

@interface TotalViewController ()<UITableViewDataSource,UITableViewDelegate>{
    DBManager *db;
    NSMutableArray* listArray;
    NSMutableArray *dateArray;
    NSMutableArray *timeArray;
    NSDateFormatter* formatter;
    
    NSString *periodStr;
    
    __weak IBOutlet UIButton *previousbtnAct;
    __weak IBOutlet UIButton *nextbtnAct;

}
@property (nonatomic, strong) NSDate *monthShowing;
@property (weak, nonatomic) IBOutlet UITableView *totalTableView;
@end

@implementation TotalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    db = [[DBManager alloc] init];
    listArray = [[NSMutableArray alloc] init];
    dateArray = [[NSMutableArray alloc] init];
    timeArray = [[NSMutableArray alloc] init];
    formatter = [[NSDateFormatter alloc] init];
    
    dateArray=[NSMutableArray arrayWithObjects:@"Period",@"Days Worked",@" ",@"Hours Worked(Total)",@"Hours Worked(Average)",@" ",@"Overtime(Total)",@"Overtime(Average)",@" ",@"Salary (Total)", nil];
    
    self.totalTableView.tableFooterView=[[UIView alloc] initWithFrame:CGRectZero];
    
//    CGRect frame = self.totalTableView.bounds;
//    frame.size.height = 72 * dateArray.count+400;
//    [self.totalTableView setFrame:frame];

    self.totalTableView.delegate=self;
    [self updateButtonStatus];

    self.title = @"Total";
    // Do any additional setup after loading the view from its nib.
}

-(void) getcurrentdate{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSDate *date = [calendar dateFromComponents:components];
    NSRange days = [calendar rangeOfUnit:NSCalendarUnitDay
                                  inUnit:NSCalendarUnitMonth
                                 forDate:date];
    
    formatter.dateFormat = @"yyyy";
    NSString *currentyear = [formatter stringFromDate:[NSDate date]];
    
    formatter.dateFormat = @"MM";
    NSString *currentmonth = [formatter stringFromDate:[NSDate date]];
    
    [self getCallender:currentyear :currentmonth :days];
}

-(void) viewWillAppear:(BOOL)animated{
    
//    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromList"];
//    
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromHome"]) {
//        NSLog(@"herere");
//        
//        NSMutableDictionary *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"SheetData"];
//        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//            DetailViewController* detail=[[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
//            detail.sheetdict=data;
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromList"];
//            
//            [self.navigationController pushViewController:detail animated:YES];
//        }];
//    }
    self.monthShowing = [NSDate date];

    [self getcurrentdate];
    self.navigationController.navigationBarHidden=YES;

}

-(void) updateButtonStatus{
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:self.monthShowing];
    [comps setSecond:0];
    
    NSDate *oldDate = [cal dateFromComponents:comps];
    
    NSDateComponents *comps2 = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                      fromDate:[NSDate date]];
    [comps2 setSecond:0];
    NSDate *currentDate = [cal dateFromComponents:comps2];
    
    
    if ([oldDate compare:currentDate] == NSOrderedSame) {
        nextbtnAct.enabled = NO;
        previousbtnAct.enabled = YES;
    }
    else{
        nextbtnAct.enabled = YES;
        previousbtnAct.enabled = YES;
        
    }
}


- (IBAction)previousbtnAct:(id)sender {
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:self.monthShowing];
    comps.month             -= 1;
    self.monthShowing          = [cal dateFromComponents:comps];
    
    NSRange days = [cal rangeOfUnit:NSCalendarUnitDay
                             inUnit:NSCalendarUnitMonth
                            forDate:self.monthShowing];
    
    formatter.dateFormat = @"yyyy";
    NSString *currentyear = [formatter stringFromDate:self.monthShowing];
    
    formatter.dateFormat = @"MM";
    NSString *currentmonth = [formatter stringFromDate:self.monthShowing];
    
    [self getCallender:currentyear :currentmonth :days];
  
    [self updateButtonStatus];
}

- (IBAction)nextbtnAct:(id)sender {
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                                     fromDate:self.monthShowing];
    comps.month             += 1;
    self.monthShowing          = [cal dateFromComponents:comps];
    
    NSRange days = [cal rangeOfUnit:NSCalendarUnitDay
                             inUnit:NSCalendarUnitMonth
                            forDate:self.monthShowing];
    
    formatter.dateFormat = @"MMM";
    NSString *currentMonth = [formatter stringFromDate:self.monthShowing];
    periodStr = [NSString stringWithFormat:@"%@/1 - %@/%lu",currentMonth,currentMonth,(unsigned long)days.length];
    
    [listArray addObject:periodStr];
    
    formatter.dateFormat = @"yyyy";
    NSString *currentyear = [formatter stringFromDate:self.monthShowing];
    
    formatter.dateFormat = @"MM";
    NSString *currentmonth = [formatter stringFromDate:self.monthShowing];
    
    [self getCallender:currentyear :currentmonth :days];
    [self updateButtonStatus];

}


-(void) getCallender : (NSString *) year : (NSString *) month : (NSRange) days{
    
    if (listArray) {
        [listArray removeAllObjects];
    }
    
//    for (int i = 1;i<=days.length;i++){
//        
//        NSString *myDateString = [NSString stringWithFormat:@"%@-%@-%d",year,month,i];
//        // Convert the string to NSDate
//        formatter.dateFormat = @"yyyy-MM-dd";
//        NSDate *date = [formatter dateFromString:myDateString];
//        // Extract the day number (14)
//        NSDateComponents *comp = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:date];
//        NSInteger day = [comp day];
//        // Extract the day name (Sunday)
//        formatter.dateFormat = @"EEE";
//        NSString *dayName = [formatter stringFromDate:date];
//        // Print
//        NSLog(@"Day: %ld: Name: %@", (long)day, dayName);
//        [dateArray addObject:[NSString stringWithFormat:@"%ld(%@)",(long)day,dayName]];
//        
//    }
    formatter.dateFormat = @"yyyy";
    NSString *currentyear = [formatter stringFromDate:self.monthShowing];
    
    formatter.dateFormat = @"MMMM";
    NSString *currentmonth = [formatter stringFromDate:self.monthShowing];
    
    formatter.dateFormat = @"MMM";
    NSString *currentMonth2 = [formatter stringFromDate:self.monthShowing];
    periodStr = [NSString stringWithFormat:@"%@/1 - %@/%lu",currentMonth2,currentMonth2,(unsigned long)days.length];
    
    [listArray addObject:periodStr];

    
    NSArray *data = [db fetchList:currentmonth :currentyear];
    
    NSString *dayCount;
    dayCount = [NSString stringWithFormat:@"%ludays", (unsigned long)data.count];

    if (data.count<= 1) {
        dayCount = [NSString stringWithFormat:@"%luday", (unsigned long)data.count];

    }
    
    [listArray addObject:dayCount];
    [listArray addObject:@" "];
    [self totalWorked:data];    
}


-(void) totalWorked : (NSArray *) data{
    
    NSCalendar *cal         = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    if (timeArray.count) {
        [timeArray removeAllObjects];
    }
    for (TimeSheet *info in data) {
      
    comps= [cal components:NSCalendarUnitSecond
                                     fromDate:info.checkinDate];
    NSDate * current=[info.checkinDate dateByAddingTimeInterval:-comps.second];
    
    comps = [cal components:NSCalendarUnitSecond
                   fromDate:info.checkoutDate];
    NSDate * current2=[info.checkoutDate dateByAddingTimeInterval:-comps.second];
    
    NSTimeInterval secondsBetween = (ceil([current2 timeIntervalSinceDate:current]));
        
    [timeArray addObject:[NSString stringWithFormat:@"%ld",(long) secondsBetween]];
    }
    
    NSInteger ti=0;
    for (int i=0; i<timeArray.count; i++) {
        ti += [[timeArray objectAtIndex:i] intValue];

    }
    NSLog(@"%ld",(long)ti);
    
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);

    [listArray addObject:[NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes]];
    
     minutes = ((ti/2) / 60) % 60;
     hours = ((ti/2) / 3600);
    
    [listArray addObject:[NSString stringWithFormat:@"%02ld:%02ld", (long)hours, (long)minutes]];
    [listArray addObject:@" "];
    
    [listArray addObject:@"00:00"];
    [listArray addObject:@"00:00"];
    [listArray addObject:@" "];
    
    NSString *numbers = [[listArray objectAtIndex:1] stringByTrimmingCharactersInSet:[NSCharacterSet letterCharacterSet]];
    [listArray addObject:numbers];
    NSLog(@"%@",listArray);
    
    [self.totalTableView reloadData];

}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor =[UIColor colorWithRed:(30/256.0) green:(30/256.0) blue:(32/256.0) alpha:(1.0)];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 72;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return dateArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        cell = (ListCell *)[topLevelObjects objectAtIndex:0];
        
    }
    
    cell.datelbl.text=[dateArray objectAtIndex:indexPath.row];
    [cell.memoImg setHidden:YES];
    cell.timelbl.text = [listArray objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryNone;

    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:(47/256.0) green:(48/256.0) blue:(52/256.0) alpha:(1.0)];
    [cell setSelectedBackgroundView:bgColorView];

    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


/*

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    [tableView setEditing:YES animated:YES];
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    for (TimeSheet *info in listArray) {
        NSArray * arr;
        arr = [[dateArray objectAtIndex:indexPath.row] componentsSeparatedByString:@"("];
        NSString* cellLabel=[arr objectAtIndex:0];
        arr = [info.date componentsSeparatedByString:@"/"];
        NSString* infodate=[arr objectAtIndex:1];
        if ([infodate hasPrefix:@"0"]) {
            infodate = [infodate stringByReplacingOccurrencesOfString: @"0" withString:@""];
        }
        
        if ([infodate isEqualToString:cellLabel]) {
            DetailViewController* detail=[[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
            detail.sheet=info;
            [self.navigationController pushViewController:detail animated:YES];
        }
    }
}

-(void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
*/
@end
