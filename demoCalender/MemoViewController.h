//
//  MemoViewController.h
//  demoCalender
//
//  Created by Niajul Hasan on 12/22/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MemoViewController : UIViewController<UITextViewDelegate>

@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *placeText;
@property (nonatomic) BOOL fromDetail;
@end
