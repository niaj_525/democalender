//
//  TimeSheet+CoreDataProperties.h
//  demoCalender
//
//  Created by Niajul Hasan on 12/22/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "TimeSheet.h"

NS_ASSUME_NONNULL_BEGIN

@interface TimeSheet (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *date;
@property (nullable, nonatomic, retain) NSString *checkinTime;
@property (nullable, nonatomic, retain) NSString *checkoutTime;
@property (nullable, nonatomic, retain) NSDate *pauseInTime;
@property (nullable, nonatomic, retain) NSDate *pauseOutTime;
@property (nullable, nonatomic, retain) NSString *memo;
@property (nullable, nonatomic, retain) NSString *monthName;
@property (nullable, nonatomic, retain) NSString *yearName;
@property (nullable, nonatomic, retain) NSDate   *checkinDate;
@property (nullable, nonatomic, retain) NSDate   *checkoutDate;
@property (nullable, nonatomic, retain) NSDictionary *pauseInfo;
@end

NS_ASSUME_NONNULL_END
