//
//  MoreViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/31/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "MoreViewController.h"
#import "DBManager.h"

@interface MoreViewController (){
    DBManager *db;
    NSString *formatedDate;
    NSString *formatedStr;
    NSDateFormatter *dateFormatter;
}

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db = [[DBManager alloc] init];
    dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *loc = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale: loc];
    self.title=[NSString stringWithFormat:@"%@ %@",self.navtitle,self.checktime];
    
    [self.datePicker setBackgroundColor:[UIColor colorWithRed:(34/256.0) green:(49/256.0) blue:(89/256.0) alpha:(1.0)]];
    [self.datePicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    // edit bar btn
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style: UIBarButtonItemStyleDone target:self action:@selector(donebtnAct:)];
    btn.possibleTitles = [NSSet setWithObjects:@"Done", nil];
    
    self.navigationItem.rightBarButtonItem = btn;
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:(250/256.0) green:(140/256.0) blue:(19/256.0) alpha:(1.0)]];
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([self.navtitle isEqualToString:@"Check-in"]) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromDetail"]) {
            NSDate *currentDate = [self.sheetdict valueForKey:@"checkinDate"];
            [self.datePicker setDate:currentDate  animated:YES];
            [self.datePicker setMaximumDate:currentDate];
            NSCalendar *gregorian = [NSCalendar currentCalendar];
            
            NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: currentDate];
            [components setHour: 12];
            [components setMinute: 00];
            //[components setSecond: 0];
            NSDate *startDate = [gregorian dateFromComponents: components];
            [self.datePicker setMinimumDate:startDate];

        }
        else{
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellEmptied"]){
                [self.datePicker setDate:[NSDate date]  animated:YES];

            }
            else{
                [self.datePicker setDate:self.sheet.checkinDate  animated:YES];
                [self.datePicker setMaximumDate:self.sheet.checkinDate];
                //NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
                NSCalendar *gregorian = [NSCalendar currentCalendar];
                
                NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: self.sheet.checkinDate];
                [components setHour: 12];
                [components setMinute: 00];
                //[components setSecond: 0];
                NSDate *startDate = [gregorian dateFromComponents: components];
                [self.datePicker setMinimumDate:startDate];
            }
        }
        
    }
    else if([self.navtitle isEqualToString:@"Check-out"]){
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromDetail"]) {
            NSDate *currentDate2 = [self.sheetdict valueForKey:@"checkoutDate"];
            [self.datePicker setDate:currentDate2  animated:YES];
            [self.datePicker setMinimumDate:currentDate2];

        }
        else{
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"cellEmptied"]){
                [self.datePicker setDate:[[NSDate date] dateByAddingTimeInterval:60]  animated:YES];
                
            }
            else{
            [self.datePicker setDate:self.sheet.checkoutDate animated:YES];
            [self.datePicker setMinimumDate:self.sheet.checkoutDate];
            }
//            NSCalendar *gregorian = [NSCalendar currentCalendar];
//            
//            NSDateComponents *components = [gregorian components: NSUIntegerMax fromDate: self.sheet.checkoutDate];
//            [components setHour: 12];
//            [components setMinute: 00];
//            //[components setSecond: 0];
//            NSDate *startDate = [gregorian dateFromComponents: components];
//            [self.datePicker setMaximumDate:startDate];

        }
    }
    else if([self.navtitle isEqualToString:@"Resume"]){
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromDetail"]) {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"empty"]) {
                
                [self.datePicker setDate:[self.sheetdict valueForKey:@"checkoutDate"] animated:YES];
                
            }
            else{
            NSDate *currentDate2 = [self.pauseTime valueForKey:@"pauseOutTime"];
            [self.datePicker setDate:currentDate2  animated:YES];
            }
        }
        else{
            if ([self.pauseTime valueForKey:@"pauseOutTime"]) {
                [self.datePicker setMinimumDate:self.sheet.checkinDate];
                [self.datePicker setDate:[self.pauseTime valueForKey:@"pauseOutTime"] animated:YES];
                [self.datePicker setMinimumDate:self.sheet.checkoutDate];

            }
            else
                [self.datePicker setDate:self.sheet.checkoutDate animated:YES];
        }
        
        
    }
    else {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromDetail"]) {
             if ([[NSUserDefaults standardUserDefaults] boolForKey:@"empty"]) {
                [self.datePicker setDate:[self.sheetdict valueForKey:@"checkinDate"] animated:YES];
                
            }
             else{
            NSDate *currentDate2 = [self.pauseTime valueForKey:@"pauseInTime"];
            [self.datePicker setDate:currentDate2  animated:YES];
             }
        }
        
        else{
            if ([self.pauseTime valueForKey:@"pauseInTime"]) {
                [self.datePicker setDate:[self.pauseTime valueForKey:@"pauseInTime"] animated:YES];
                
            }
            else
                [self.datePicker setDate:self.sheet.checkinDate animated:YES];
        }
    }
}

-(void) viewWillDisappear:(BOOL)animated{
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDetail"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"empty"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"cellEmptied"];

}


- (IBAction)donebtnAct:(id)sender {
    
    NSMutableDictionary* info=[[NSMutableDictionary alloc] init];
    if ([self.navtitle isEqualToString:@"Check-in"]) {
        
        NSCalendar *calendar=[NSCalendar currentCalendar];
        NSDateComponents *components;
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromDetail"]) {
            
            components = [calendar components: NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay fromDate:[self.sheetdict valueForKey:@"checkinDate"]];
        }
        else
            components = [calendar components: NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay fromDate:self.sheet.checkinDate];
        
        [dateFormatter setDateFormat:@"HH"];
        formatedDate=[dateFormatter stringFromDate:self.datePicker.date];
        components.hour=[formatedDate integerValue];
        
        [dateFormatter setDateFormat:@"mm"];
        formatedDate=[dateFormatter stringFromDate:self.datePicker.date];
        components.minute=[formatedDate integerValue];
        
        NSDate *current=[calendar dateFromComponents:components];
        
        [info setValue:formatedStr forKey:@"checkinTime"];
        [info setValue:current forKey:@"checkinDate"];
        
    }
    else if ([self.navtitle isEqualToString:@"Start Pause"]) {
        dateFormatter.dateFormat=@"H:mm";
        NSString *currentTime= [dateFormatter stringFromDate: self.datePicker.date];
        NSDate *currentDate = [dateFormatter dateFromString:currentTime];
        [[NSUserDefaults standardUserDefaults] setValue:currentDate forKey:@"pauseInTime"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isempty"];
        
        
    }else if ([self.navtitle isEqualToString:@"Resume"]) {
        NSLog(@"Resume");
        
        dateFormatter.dateFormat=@"H:mm";
        NSString *currentTime= [dateFormatter stringFromDate: self.datePicker.date];
        NSDate *currentDate = [dateFormatter dateFromString:currentTime];
        [[NSUserDefaults standardUserDefaults] setValue:currentDate forKey:@"pauseOutTime"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isempty"];
        
    }
    else{
        NSCalendar *calendar=[NSCalendar currentCalendar];
        NSDateComponents *components;
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromDetail"]) {
            
            components = [calendar components: NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay fromDate:[self.sheetdict valueForKey:@"checkoutDate"]];
        }
        else
            components = [calendar components: NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay fromDate:self.sheet.checkoutDate];
        
        [dateFormatter setDateFormat:@"HH"];
        formatedDate=[dateFormatter stringFromDate:self.datePicker.date];
        components.hour=[formatedDate integerValue];
        
        [dateFormatter setDateFormat:@"mm"];
        formatedDate=[dateFormatter stringFromDate:self.datePicker.date];
        components.minute=[formatedDate integerValue];
        
        NSDate *current=[calendar dateFromComponents:components];
        [info setValue:formatedStr forKey:@"checkOut"];
        [info setValue:current forKey:@"checkoutDate"];
        
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"empty"]) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    else{
        [db updateExistingData:self.checktime :info];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)datePicker:(id)sender {
    
    [dateFormatter setDateFormat:@"MMM/dd HH:mm"];
    formatedStr = [dateFormatter stringFromDate:self.datePicker.date];
}

- (IBAction)currentTime:(id)sender {
    
    [self.datePicker setDate:[NSDate date]];
}
@end
