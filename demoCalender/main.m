//
//  main.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/21/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
