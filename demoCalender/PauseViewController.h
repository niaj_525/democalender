//
//  PauseViewController.h
//  demoCalender
//
//  Created by Niajul Hasan on 1/2/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeSheet.h"

@interface PauseViewController : UIViewController

@property (nonatomic, strong) TimeSheet *sheet;
@property (nonatomic, strong) NSMutableDictionary *sheetdict;
@property (nonatomic, strong) NSDictionary *pauseTime;

@end
