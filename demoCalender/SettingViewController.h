//
//  SettingViewController.h
//  demoCalender
//
//  Created by Niajul Hasan on 12/27/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
