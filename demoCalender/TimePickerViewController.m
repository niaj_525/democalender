//
//  TimePickerViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 1/29/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import "TimePickerViewController.h"

@interface TimePickerViewController ()
@property (weak, nonatomic) IBOutlet UIDatePicker *timePicker;

@end

@implementation TimePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.navTitle;
    
    [self.timePicker setBackgroundColor:[UIColor colorWithRed:(34/256.0) green:(49/256.0) blue:(89/256.0) alpha:(1.0)]];
    [self.timePicker setValue:[UIColor whiteColor] forKey:@"textColor"];

    
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    
    self.navigationItem.rightBarButtonItem = save;

    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated{
    
    NSDate *date = self.pickerDate;
    NSLog(@"%@",date);
    if (date) {
        [self.timePicker setDate:self.pickerDate animated:YES];

    }
    else
        [self.timePicker setDate:[NSDate date] animated:YES];

}

-(void) viewWillDisappear:(BOOL)animated{

}

-(void) saveTapped{
    if ([self.navTitle isEqualToString:@"Starting Time"]) {
        [[NSUserDefaults standardUserDefaults] setObject:self.timePicker.date forKey:@"startingTime"];

    }
    else if ([self.navTitle isEqualToString:@"Finish Time"]) {

       [[NSUserDefaults standardUserDefaults] setObject:self.timePicker.date forKey:@"finishingTime"];
    }
    
    else{
        if ([self.breakStr isEqualToString:@"Start"]) {
            [[NSUserDefaults standardUserDefaults] setObject:self.timePicker.date forKey:@"startBreak"];
        }
        else
            [[NSUserDefaults standardUserDefaults] setObject:self.timePicker.date forKey:@"finishBreak"];
 
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)currentTime:(id)sender {
    
    [self.timePicker setDate:[NSDate date] animated:YES];
}

@end
