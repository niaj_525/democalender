//
//  SettingCell.m
//  demoCalender
//
//  Created by Niajul Hasan on 1/29/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import "SettingCell.h"

@implementation SettingCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
