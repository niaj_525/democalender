//
//  HomeViewController.h
//  demoCalender
//
//  Created by Niajul Hasan on 12/22/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListViewController.h"

@interface HomeViewController : UIViewController

@property (nonatomic, retain) ListViewController *listVC;
@end
