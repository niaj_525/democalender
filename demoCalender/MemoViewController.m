//
//  MemoViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 12/22/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import "MemoViewController.h"
#import "DBManager.h"

@interface MemoViewController (){
    DBManager *db;

}

@property (weak, nonatomic) IBOutlet UITextView *memoTextV;
@end

@implementation MemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    db=[[DBManager alloc] init];

    self.navigationItem.title=@"Memo";
    UIBarButtonItem *save=[[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(savebtnAct)];
    self.navigationItem.rightBarButtonItem=save;
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar  setBarTintColor:[UIColor blackColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:(250/256.0) green:(140/256.0) blue:(19/256.0) alpha:(1.0)]];
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];
    
    if (self.placeText) {
        self.memoTextV.text = self.placeText;
    }
}

-(void) viewWillDisappear:(BOOL)animated{
    self.fromDetail = NO;
}

-(void)savebtnAct{

    if (self.fromDetail) {
        
        NSMutableDictionary* info=[[NSMutableDictionary alloc] init];
        [info setValue:self.memoTextV.text forKey:@"memo"];
        [db updateExistingData:self.date :info];

    }else
        
        [[NSUserDefaults standardUserDefaults] setValue:self.memoTextV.text forKey:@"memo"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
