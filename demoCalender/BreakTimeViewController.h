//
//  BreakTimeViewController.h
//  demoCalender
//
//  Created by Niajul Hasan on 1/30/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BreakTimeViewController : UIViewController
@property (nonatomic, strong) NSString *navTitle;
@property (nonatomic, strong) NSString *fromWhere;
@property (nonatomic, strong) NSDate *pickerDate;
@end
