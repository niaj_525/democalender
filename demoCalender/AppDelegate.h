//
//  AppDelegate.h
//  demoCalender
//
//  Created by Niajul Hasan on 12/21/15.
//  Copyright © 2015 Niajul Hasan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#define kAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate]);

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NSMutableArray* historyArray;



@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(void)updateHistoryArray: (NSArray *) historyAr;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

