//
//  PauseViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 1/2/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import "PauseViewController.h"
#import "ListCell.h"
#import "MoreViewController.h"
#import "DBManager.h"

@interface PauseViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSDateFormatter *formatter;
    DBManager *db;
    
}

@property (weak, nonatomic) IBOutlet UITableView *pauseTable;
@end

@implementation PauseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Pause";
    self.pauseTable.delegate = self;
    formatter = [[NSDateFormatter alloc] init];
    db=[[DBManager alloc] init];
    // Do any additional setup after loading the view from its nib.
}

-(void) viewWillAppear:(BOOL)animated{
    [self.pauseTable reloadData];
}

-(void) viewWillDisappear:(BOOL)animated{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isempty"];
    //    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"pauseInTime"];
    //    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"pauseOutTime"];
    
    
}

- (IBAction)finishbtnAct:(id)sender {
    
    NSMutableDictionary* info=[[NSMutableDictionary alloc] init];
    
    NSMutableArray* array;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
        array = [NSKeyedUnarchiver unarchiveObjectWithData:(NSData *)[self.sheetdict valueForKey:@"pauseInfo"]];

    }
    else
        array = [NSKeyedUnarchiver unarchiveObjectWithData:(NSData *)self.sheet.pauseInfo];
    
    for (int i=0; i< array.count; i++) {
        NSDictionary* pauseInfo = [array objectAtIndex:i];
        NSString *str=[NSString stringWithFormat:@"%@",[pauseInfo valueForKey:@"pauseInTime"]];
        if ([str isEqualToString:@""]) {
            [array removeObjectAtIndex:i];
        }
    }

    [info setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"pauseInTime"] forKey:@"pauseInTime"];
    [info setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"pauseOutTime"] forKey:@"pauseOutTime"];
    [array addObject:info];
    
    NSData *arrayData = [NSKeyedArchiver archivedDataWithRootObject:array];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
     
        [db updatePauseData:[self.sheetdict valueForKey:@"date"] :arrayData];
        [db fetchList:[self.sheetdict valueForKey:@"monthName"] :[self.sheetdict valueForKey:@"yearName"]];

    }
    else{
    [db updatePauseData:self.sheet.date :arrayData];
    [db fetchList:self.sheet.monthName :self.sheet.yearName];
    }
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.backgroundColor =[UIColor colorWithRed:(30/256.0) green:(30/256.0) blue:(32/256.0) alpha:(1.0)];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 72;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ListCell" owner:self options:nil];
        cell = (ListCell *)[topLevelObjects objectAtIndex:0];
        
    }
    cell.memoImg.hidden=YES;
    
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromDetail"]) {
        
        formatter.dateFormat = @"H:mm";
        NSString *formatted=[formatter stringFromDate:[self.pauseTime valueForKey:@"pauseInTime"]];
        NSString *formatted2=[formatter stringFromDate:[self.pauseTime valueForKey:@"pauseOutTime"]];
        
        if (indexPath.row==0) {
            
            cell.datelbl.text=@"Start Pause";
            if (formatted)
                cell.timelbl.text =formatted;
            else
                cell.timelbl.text =@"";
            
        }
        if (indexPath.row==1) {
            
            cell.datelbl.text=@"Resume";
            if (formatted)
                cell.timelbl.text =formatted2;
            else
                cell.timelbl.text =@"";
            
        }
    }else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isempty"]){
        
        if (indexPath.row==0) {
            
            cell.datelbl.text=@"Start Pause";
            if ([[NSUserDefaults standardUserDefaults] valueForKey:@"pauseInTime"]) {
                cell.timelbl.text =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pauseInTime"] ];
                
            }
            else
                cell.timelbl.text =@"";
        }
        else{
            cell.datelbl.text=@"Resume";
            if ([[NSUserDefaults standardUserDefaults] valueForKey:@"pauseOutTime"]) {
                cell.timelbl.text =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"pauseOutTime"]];
                
            }
            else
                cell.timelbl.text =@"";
            
        }
        
    }
    else{
        
        formatter.dateFormat = @"H:mm";
        NSString *formatted=[formatter stringFromDate:[self.pauseTime valueForKey:@"pauseInTime"]];
        NSString *formatted2=[formatter stringFromDate:[self.pauseTime valueForKey:@"pauseOutTime"]];
        
        if (indexPath.row==0) {
            
            cell.datelbl.text=@"Start Pause";
            if (formatted)
                cell.timelbl.text =formatted;
            else
                cell.timelbl.text =@"";
            
        }
        if (indexPath.row==1) {
            
            cell.datelbl.text=@"Resume";
            if (formatted)
                cell.timelbl.text =formatted2;
            else
                cell.timelbl.text =@"";
            
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:(47/256.0) green:(48/256.0) blue:(52/256.0) alpha:(1.0)];
    [cell setSelectedBackgroundView:bgColorView];

    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //    NSLog(@"%@",self.sheetdict);
    ListCell *cell = [self.pauseTable cellForRowAtIndexPath:indexPath];
    
    if (indexPath.row==0 || indexPath.row==1) {
        
        MoreViewController* detail=[[MoreViewController alloc] initWithNibName:@"MoreViewController" bundle:nil];
        if (indexPath.row==0) {
            
            //            detail.navtitle = @"Check-in";
            detail.navtitle = @"Start Pause";
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
                detail.checktime = [self.sheetdict valueForKey:@"date"];
                if ([cell.timelbl.text isEqualToString:@""]) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"empty"];
                    detail.checktime = @"today";
                    
                }
                
            }
            
            else{
                detail.checktime = self.sheet.date;
                if ([cell.timelbl.text isEqualToString:@""]) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"empty"];
                    detail.checktime = @"today";
                    
                }
            }
            
        }
        if (indexPath.row==1) {
            //            detail.navtitle = @"Check-out";
            detail.navtitle = @"Resume";
            
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
                detail.checktime = [self.sheetdict valueForKey:@"date"];
                if ([cell.timelbl.text isEqualToString:@""]) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"empty"];
                    detail.checktime = @"today";
                    
                }
                
            }
            
            else{
                detail.checktime = self.sheet.date;
                if ([cell.timelbl.text isEqualToString:@""]) {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"empty"];
                    detail.checktime = @"today";
                    
                }
            }
            
        }
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"fromList"]) {
            
            detail.sheetdict=self.sheetdict;
            detail.pauseTime = self.pauseTime;
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"fromDetail"];
            
        }
        else{
            detail.sheet=self.sheet;
            detail.pauseTime = self.pauseTime;
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"fromDetail"];
            
        }
        
        [self.navigationController pushViewController:detail animated:YES];
        
    }
}

@end
