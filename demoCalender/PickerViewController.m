//
//  PickerViewController.m
//  demoCalender
//
//  Created by Niajul Hasan on 1/30/16.
//  Copyright © 2016 Niajul Hasan. All rights reserved.
//

#import "PickerViewController.h"

@interface PickerViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>{
    int dataCount;
    NSString *selectedStr;
}

@property (weak, nonatomic) IBOutlet UIPickerView *pickView;

@end

@implementation PickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pickView.delegate = self;

    self.navigationItem.title = self.navTitle;
    UIBarButtonItem *save = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    
    self.navigationItem.rightBarButtonItem = save;

}

-(void) viewWillAppear:(BOOL)animated{
    dataCount = [self getMonthRange];
}

-(int) getMonthRange{
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSDate *date = [calendar dateFromComponents:components];
    NSRange days = [calendar rangeOfUnit:NSCalendarUnitDay
                                  inUnit:NSCalendarUnitMonth
                                 forDate:date];
    
    return (int)days.length;
    
}

-(void) saveTapped{
    //    if ([self.navTitle isEqualToString:@"Starting Time"]) {
    ////        [[NSUserDefaults standardUserDefaults] setObject:self.timePicker.date forKey:@"startingTime"];
    //
    //    }
    //    else
    [[NSUserDefaults standardUserDefaults] setValue:selectedStr forKey:@"deadlineDate"];
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return dataCount;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    if (row < dataCount-1) {
        return [NSString stringWithFormat:@"%ld",(long)row+1];
    }
    return @"month end";
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    selectedStr = [self pickerView:pickerView titleForRow:row forComponent:0];
}
@end
